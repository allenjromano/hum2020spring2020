---
title: FAQ
weight: 20
---

# Frequently Asked Questions, aka "What should I do if...."

## General advice
If you have a question about anything, you should, in general, do the following:

 1. Check the course homepage. See if there are relevant announcements, tips, or anything in the ticker.
2. Check the syllabus.
3. Check the specific assignment. There's a lot of verbiage there. Often your answer is waiting there.

If none of these methods resolves your problem, then get in touch with me via the methods laid out on the homepage.

And, as a general rule, the answer is always, **don't panic** and then come and talk to me about it if necessary.

## Technical Questions

For now, get in touch with me. Over the first week we will go through how to do assignments and get all the technical logistics of the class in place.
This class is designed to use minimal tools so that ultimately all you need to do is to submit files to a folder. The TAs and I will take it from there.

## Sickness

In general, for short illness, all the material is online and so you can check back in when you are feeling better. If you are sick, rest up and take care of yourself. No need to be a hero (your peers will thank you for not getting them sick too.) 

If you find yourself sick for an extended period, then get in touch so that we can have a plan for completing any work. 

In accordance with the university absence policy, accommodation for illness requires documentation. 

{{</* tabs "sickness" */>}}
{{</* tab "A single class" */>}} # If you miss a single class...
due to illness then you should not worry about it. An occasional missed class due to illness is not unusual and is built into the grading system. It will only be an issue if it happens a lot. 
{{</* /tab */>}}
{{</* tab "many days" */>}} # If illness lasts many classes...
get in touch with me. In general I will tell you to rest up first, take crae of yourself, and that we will deal with any of the work once you have recovered or are able to do so. 
{{</* /tab */>}}
{{</* tab "Serious illness" */>}} # In case of serious illness ...
that takes priority. Get in touch when you can and we'll figure out what needs to be done from an academic perspective. Also, in this case I would highly recommend that you get in touch with the Dean of Students office as soon as possible. They can both connect you to relevant resources on campus and also give official documentation to all of your professors at once.
{{</* /tab */>}}
{{</* /tabs */>}}


## Confusion
If you find yourself confused about something, then get in touch. More often than not you'll be helping answer a question for other people who may have been confused too but not spoken up.


## Excitement
This can happen too. In this case, I'm happy to provide further fodder for exploration.

## Panic

I told you not to panic. Get in touch with me. There is never any reason to panic in this class. I have no interest in giving out 'F's. (so, if you find yourself thinking "oh no, I just failed", then don't think that. Set up an appointment to meet instead.)
