---
title: "Week 3 Overview"
---

This week we start a blazing fast tour from antiquity to Modernity. We start in ancient Greece and ask questions about the kinds of ideas the ancients had about life and the world around them. 
